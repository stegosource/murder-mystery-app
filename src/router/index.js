import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import ModeratorScreen from '@/components/ModeratorScreen'
import CustomizeGame from '@/components/CustomizeGame'
import SinglePlayer from '@/components/SinglePlayer'
import GameScreen from '@/components/GameScreen'
import store from '@/store';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/moderator',
      name: 'ModeratorScreen',
      component: ModeratorScreen
    },
    {
      path: '/customize-game',
      name: 'CustomizeGame',
      component: CustomizeGame
    },
    {
      path: '/player/:id',
      name: 'SinglePlayer',
      component: SinglePlayer,
      props: true
    },
    {
      path: '/play',
      name: 'GameScreen',
      component: GameScreen,
      props: true
    }
  ]
})

// https://router.vuejs.org/en/advanced/navigation-guards.html
router.beforeEach((to, from, next) => {
  if (to.path === '/' || JSON.parse(sessionStorage.getItem("players"))) {
    next();
  } else {
    next('/');
  }
});

export default router;
import Vue from 'vue';
import Vuex from 'vuex';
import themes from './themes';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    isGameStarted: false,
    players: [],
    activeTheme: 'standard',
    roles: [
      { name: 'citizen', description: '', isAvailable: true, isActive: true },
      { name: 'mafia', description: '', isAvailable: true, isActive: true },
      { name: 'detective', description: '', isAvailable: true, isActive: true },
      { name: 'doctor', description: '', isAvailable: true, isActive: true },
    ],
    themes
  },
  mutations: {
    EDIT_PLAYER: (state, payload) => {
      state.players.splice(payload.index, 1, payload.newPlayer);
    },
    ASSIGN_ROLES: state => {
      let availableRoles;
      // There should be 1 mafia member per 3 villagers, rounded down
      switch (state.players.length - 1) {
        case 4:
          availableRoles = ['citizen', 'citizen', 'mafia', 'detective'];
          break;
        case 5:
          availableRoles = ['citizen', 'citizen', 'mafia', 'mafia', 'detective'];
          break;
        case 6:
          availableRoles = ['citizen', 'citizen', 'mafia', 'mafia', 'detective', 'doctor'];
          break;
        case 7:
          availableRoles = ['citizen', 'citizen', 'citizen', 'mafia', 'mafia', 'detective', 'doctor'];
          break;
        case 8:
          availableRoles = ['citizen', 'citizen', 'citizen', 'citizen', 'mafia', 'mafia', 'detective', 'doctor'];
          break;
        case 9:
          availableRoles = ['citizen', 'citizen', 'citizen', 'citizen', 'mafia', 'mafia', 'mafia', 'detective', 'doctor'];
          break;
        case 10:
          availableRoles = ['citizen', 'citizen', 'citizen', 'citizen', 'mafia', 'mafia', 'mafia', 'detective', 'detective', 'doctor'];
          break;
        case 11:
          availableRoles = ['citizen', 'citizen', 'citizen', 'citizen', 'mafia', 'mafia', 'mafia', 'detective', 'detective', 'doctor', 'doctor'];
          break;
      }
      for (let i = 0; i < state.players.length; i++) {
        if (i === 0) {
          // First player is always the moderator
          state.players[0].role = 'moderator';
        } else {
          // Get random role
          const role = Math.floor(Math.random() * availableRoles.length);
          // Assign to a player
          state.players[i].role = availableRoles[role];
          // Remove the role from the list
          availableRoles.splice(role, 1);
        }
      }
    },
    TOGGLE_GAME_STATUS: (state) => {
      state.isGameStarted = true;
    },
    CHANGE_THEME: (state, payload) => {
      state.activeTheme = payload;
    }
  },
  actions: {
    editPlayer: ({ commit }, payload) => {
      commit('EDIT_PLAYER', payload);
    },
    assignRoles: ({ commit }) => {
      commit('ASSIGN_ROLES');
    },
    toggleGameStatus: ({ commit }) => {
      commit('TOGGLE_GAME_STATUS');
    },
    changeTheme: ({ commit }, payload) => {
      commit('CHANGE_THEME', payload);
    }
  },
  getters: {
    players: state => {
      return state.players;
    }
  },
  modules: {

  }
});

export default store
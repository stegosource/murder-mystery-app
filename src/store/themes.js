const themes = [
	{
		name: 'standard',
		isAvailable: true,
		description: 'a very basic theme',
		customRoles: [],
		killStories: [],
		saveStories: []
	},
	{
		name: 'second',
		isAvailable: false,
		description: 'a super awesome theme' 
	},
	{
		name: 'third',
		isAvailable: true,
		description: 'This theme is alright'
	},
	{
		name: 'standard2',
		isAvailable: true,
		description: 'a very basic theme'
	},
	{
		name: 'second2',
		isAvailable: false,
		description: 'a super awesome theme' 
	},
	{
		name: 'third2',
		isAvailable: true,
		description: 'This theme is alright'
	},
	{
		name: 'standard3',
		isAvailable: true,
		description: 'a very basic theme'
	},
	{
		name: 'second3',
		isAvailable: false,
		description: 'a super awesome theme' 
	},
	{
		name: 'third3',
		isAvailable: true,
		description: 'This theme is alright'
	}
]

export default themes;